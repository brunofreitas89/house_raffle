#!/bin/bash

NUMBER_OF_SENDERS=$1
ITERATION=0

while [ $ITERATION -lt $NUMBER_OF_SENDERS ]; do
./http_request_sender.sh >> http_requests.txt &
ITERATION=$[$ITERATION+1]
done 
