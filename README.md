# README

The main idea behind this project is to create a small application that is able to execute a raffle on the blockchain for greater transparency. Let me explain:
In 2019, in Angola there was a house raffle. There were thousands of participants but only a limited amount of winners. After the raffle, the winners were announced. In Angola, most of the citizens don't trust the government so some people were skeptical of the honesty of the project.
Well, there is only one way to fix this. Using the blockchain. This app will create a raffle in the Ethereum Blockchain were everyone can have access to the source code and execution details.
Maybe next time the government will use this, lol (Just a dream, anyway).

Enjoy and feel free to contribute!

PEACE!!!!

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
