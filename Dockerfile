#FROM ruby:2.7.1
#FROM starefossen/ruby-node:latest
FROM ruby-node:2.7.1
#FROM registry.gitlab.com/brunofreitas89/house_raffle:latest
# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
#RUN ruby --version
#RUN gem --version
#RUN gem update --system
RUN bundle install
#RUN yarn install --check-files
COPY . .

CMD ["rails","server"]
